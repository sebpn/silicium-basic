<div id="top"></div>

<div align="center">
  <a href="https://gitlab.com/sebpn/silicium-basic">
    <img src="./resources/bjorn_logo.png" alt="Silicium Bjorn">
  </a>
  <h1 align="center">Silicium - BASIC</h1>
</div>

**Table des matières**

[TOC]

## A propos du projet

Ce projet présente les projets en BASIC sur lesquels j'ai contribués.

# Les projets en GW Basic

## Evènements

* `silinoel.bas` : Joyeux Noël :christmas_tree:
  
  ![](./resources/silinoel_bas.gif)

* `silinvan.bas` : Bonne année :champagne:
  
  ![](./resources/silinvan_bas.gif)

* ...

<div align="right">(<a href="#top">back to top</a>)</div>

## Silicium Whizz machines

![](./resources/banniere.png)

Silicium Whizz Machines est la nouvelle exposition de Silicium. Elle se présente comme un escape game simplifié qui permettra aux visiteurs de découvrir des machines anciennes : jeux vidéo sur console, manipulation d’ordinateurs et autres équipements désuets.

### V 2.0.0

* `whizcii.bas` : ASCII Art Silicium Whizz machines

### V 1.0.0

Ce projet présente les 4 modules que j'ai réalisés en GW-Basic. 
Ainsi que ma contribution pour l'intégration de ces modules dans le programme complet et qq corrections.

* `2QLNbre.bas` : 3 essais pour retrouver un nombre entre 1 et 10

* `3Tests.bas` : Simulation état processeur, mémoire, sécurité

* `4Mascii.bas` : ASCII Art d'une machine (UC, écran, clavier, etc...)

* `4Concep.bas` : Conception d'une WHIZZ machine Silicium

* `5Transf.bas` : Transfert entre 2 barres de progression

* `WHIZZ.BAS` : Le programme complet de la simulation

<div align="right">(<a href="#top">back to top</a>)</div>

# Contact

![avatar](https://gitlab.com/uploads/-/system/user/avatar/5863632/avatar.png?width=23) Sébastien Périn - sebastien.perin@gmail.com

Project Link: [https://gitlab.com/sebpn/silicium-basic](https://gitlab.com/sebpn/silicium-basic)

<div align="right">(<a href="#top">back to top</a>)</div>
