10 REM ***********************************************
20 REM ASCII ART - PROGRAMME GW BASIC
30 REM Auteurs : PERIN S.
40 REM Silicium 2023
50 REM ************************************************
60 REM Site text->ascii  : https://ascii-generator.site/t/
70 REM Parametres        : doom
80 REM ************************************************
90 duree=3000 
100 CLS:PRINT  "Silicium Machines & Signaux vous souhaite ..."
110 FOR j= 1 TO duree: NEXT j
120 PRINT "     _   _                 ______                                 " 
130 PRINT "    | | | |                | ___ \                                "
140 PRINT "    | | | | _ __    ___    | |_/ /  ___   _ __   _ __    ___      "
150 PRINT "    | | | || '_ \  / _ \   | ___ \ / _ \ | '_ \ | '_ \  / _ \     "
160 PRINT "    | |_| || | | ||  __/   | |_/ /| (_) || | | || | | ||  __/     "
170 PRINT "     \___/ |_| |_| \___|   \____/  \___/ |_| |_||_| |_| \___|     "
180 PRINT "        _       _   _                                             "
190 PRINT "       | |     | | | |                                            "
200 PRINT "   ___ | |_    | |_| |  ___  _   _  _ __   ___  _   _  ___   ___  "
210 PRINT "  / _ \| __|   |  _  | / _ \| | | || '__| / _ \| | | |/ __| / _ \ "
220 PRINT " |  __/| |_    | | | ||  __/| |_| || |   |  __/| |_| |\__ \|  __/ "
230 PRINT "  \___| \__|   \_| |_/ \___| \__,_||_|    \___| \__,_||___/ \___| "                                                        
240 PRINT "   ___                                _____  _____  _____    ___  "
250 PRINT "  / _ \                  /           / __  \|  _  |/ __  \  /   | "
260 PRINT " / /_\ \ _ __   _ __    ___   ___    `' / /'| |/' |`' / /' / /| | "
270 PRINT " |  _  || '_ \ | '_ \  / _ \ / _ \     / /  |  /| |  / /  / /_| | "
280 PRINT " | | | || | | || | | ||  __/|  __/   ./ /___\ |_/ /./ /___\___  | "
290 PRINT " \_| |_/|_| |_||_| |_| \___| \___|   \_____/ \___/ \_____/    |_/ "
300 FOR i=0 TO 20
310 IF (i/2) = INT(i/2) THEN P$=" " ELSE P$="*"
320 LOCATE 3, 21, 0 : PRINT P$; : LOCATE 3, 56, 0 : PRINT P$;
330 LOCATE 8, 3, 0 : PRINT P$; : LOCATE 8, 63, 0 : PRINT P$;
340 LOCATE 15, 15, 0 : PRINT P$; : LOCATE 15, 35, 0 : PRINT P$;
350 FOR j= 1 TO duree: NEXT j : NEXT i
360 LOCATE 23, 1
