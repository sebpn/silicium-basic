10 REM ***********************************************
20 REM ASCII ART - PROGRAMME GW BASIC
30 REM Auteurs : PERIN S.
40 REM Silicium 2023
50 REM ************************************************
60 REM Site image->ascii : https://ascii-generator.site/ 
70 REM Parametres        : 1, col=79, bght=100, ctrst=150 
75 REM
80 REM Site text->ascii  : https://ascii-generator.site/t/ 
90 REM Parametres        : swan
100 REM ************************************************
110 duree=3000
120 CLS:PRINT  "Silicium Machines & Signaux vous souhaite ..."
130 FOR j= 1 TO duree: NEXT j  
140 PRINT "                                                          .                    "
150 PRINT ".   .        .---.                                      :---:                  "
160 PRINT "|   |            |                                   :-----====-:              "
170 PRINT "|   |.--.        | .-. .  . .-. .  . -. ,              --=====                 "
180 PRINT ":   ;|  |        ;(   )|  |(.-' |  |   :               -=+*++=:                "
190 PRINT " `-' '  `-   `--'  `-' `--| `--'`--`--' `-             :=##*#=:                "
200 PRINT "                          ;                            -#*#*+*-                "
210 PRINT "                       `-'                           .+#=*#####*:              "
220 PRINT "            .   .          .                        +*##########+              "
230 PRINT "            |\  |      o o |                      .-+***=*#######*-            "
240 PRINT "            | \ | .-.  .-. |                    -*%%%%%%%#+*%%%%@%%+           "
250 PRINT "            |  \|(   )(.-' |                   ::=%@%%@%%+:-%%%%%%%%:          "
260 PRINT "            '   ' `-'  `--'`-                  .#@@%%@@@@@@@@@@@@@%%%+         "
270 PRINT "                                             -*%%#+-+*%%@%%%%%%%%#%%%#+.       "
280 PRINT "         .-.    .          .                .-*%%=---+%%%%@%%%%%@@%%%%%=       "
290 PRINT "        (   )   |      o  _|_                 =%@@#*%@@@@@+@@@@@@#%@@@@#:      "
300 PRINT "         >-<    |.-.   .   |  .--.         :+%@@@@#*%@@@@@@@@@@@@@@@@@@@@%+:   "
310 PRINT "        (   )   |   )  |   |  `--.       -*#%@@@@@@@@@%@@@@+--+@%==%@@@@@@@**- "
320 PRINT "         `-'    '`-' -' `- `-'`--'       -==-: :+-#@@@@#-%@@@@@%#@@@%--. .---- "
330 PRINT "                                                         @@@@#:                "
340 PRINT "                                                        -@@@@@                 "
350 FOR i=0 TO 20
360 IF (i/2) = INT(i/2) THEN P$=" " : IP$="*" ELSE P$="*" : IP$=" "
370 LOCATE 3, 56, 0 : PRINT P$; : LOCATE 3, 62, 0 : PRINT P$;
380 LOCATE 5, 55, 0 : PRINT P$; : LOCATE 5, 63, 0 : PRINT P$;
390 LOCATE 6, 59, 0 : PRINT IP$; : LOCATE 8, 62, 0 : PRINT IP$;
400 LOCATE 9, 58, 0 : PRINT IP$; : LOCATE 9, 64, 0 : PRINT IP$;
410 LOCATE 11, 54, 0 : PRINT IP$; : LOCATE 11, 58, 0 : PRINT IP$; 
420 LOCATE 11, 66, 0 : PRINT IP$;
430 LOCATE 15, 47, 0 : PRINT IP$; : LOCATE 15, 54, 0 : PRINT IP$;
440 LOCATE 19, 43, 0 : PRINT IP$; : LOCATE 19, 76, 0 : PRINT IP$;
450 LOCATE 20, 47, 0 : PRINT P$; : LOCATE 20, 73, 0 : PRINT P$;
460 FOR j= 1 TO duree: NEXT j : NEXT i
470 LOCATE 23, 1
