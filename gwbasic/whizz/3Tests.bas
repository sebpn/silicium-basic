30000 CLS
30010 REM ***********************************************
30011 REM TESTS - PROGRAMME GW BASIC
30012 REM Auteurs : PERIN S.
30013 REM Silicium 2023
30014 REM ************************************************

30020 RANDOMIZE TIMER
30030 duree=1000 : BARMAX% = 26 : MEMMAX% = 16
30040 CLS:PRINT "3. Tests de l'";CHR$(130);"tat de la m";CHR$(130);"moire centrale et du processeur": PRINT
30050 REM TEXTES FIXES
30060 PRINT "Processeur"
30070 PRINT SPACE$(2);"Utilisation : ";TAB(17);"[ ";        : LPUS%=CSRLIN : CPUS%=POS(0) : PRINT TAB(45);" ] ";SPACE$(3);TAB(54);"%"
30080 PRINT SPACE$(2);"Tension : ";TAB(49);                 : LPTS%=CSRLIN : CPTS%=POS(0) : PRINT SPACE$(4);TAB(54);"V"
30090 PRINT SPACE$(2);"Fr";CHR$(130);"quence : ";TAB(49);   : LPFQ%=CSRLIN : CPFQ%=POS(0) : PRINT SPACE$(4);TAB(54);"MHz" 
30100 PRINT SPACE$(2);"Temp";CHR$(130);"rature : ";TAB(49); : LPTP%=CSRLIN : CPTP%=POS(0) : PRINT SPACE$(4);TAB(54);CHR$(248);"C"
30110 PRINT "M";CHR$(130);"moire"
30120 PRINT SPACE$(2);"Totale : ";TAB(49); : PRINT USING "##";MEMMAX%; : PRINT TAB(54);"Ko"
30130 PRINT SPACE$(2);"Utilisation : ";TAB(17);"[ ";        : LMUS%=CSRLIN : CMUS%=POS(0) : PRINT TAB(45);" ] ";SPACE$(3);TAB(54);"Ko"
30140 PRINT "Cybers";CHR$(130);"curit";CHR$(130)
30150 PRINT SPACE$(2);"Base de virus : ";TAB(19);"[ ";      : LVIR%=CSRLIN : CVIR%=POS(0) : PRINT TAB(45);" ] ";SPACE$(3);TAB(54);"%"
30160 PRINT SPACE$(2);"Base de vers : ";TAB(19);"[ ";       : LVER%=CSRLIN : CVER%=POS(0) : PRINT TAB(45);" ] ";SPACE$(3);TAB(54);"%"
30170 PRINT SPACE$(2);"Base de ran";CHR$(135);"ongiciels : ";TAB(27);"[ "; : LRNC%=CSRLIN : CRNC%=POS(0) : PRINT TAB(45);" ] ";SPACE$(3);TAB(54);"%"
30180 REM VALEURS 
30190 ratioP% = INT( 1 + RND * 100 )
30200 BP% = INT(ratioP% * BARMAX% / 100) : IF BP% < 1 THEN BP% = 1 ELSE BP% = BP% + 1
30210 ratioM% = INT( 1 + RND * 100 )
30220 BM% = INT(ratioM% * BARMAX% / 100) : IF BM% < 1 THEN BM% = 1 ELSE BM% = BM% + 1
30230 FOR i=0 TO BARMAX%
30240 tension! = 4.5 + CSNG( (5.5 - 4.5) * RND) : REM tension = [4.5 ; 5.5] Volts
30250 frequence! =  1.8 + CSNG( (2.2 - 1.8) * RND) : REM frequence = [1.8 ; 2.2] MHz
30260 tempmax%=30.0: IF tension!>5.0 THEN tempmax%=tempmax%+10 : IF frequence!>2.0 THEN tempmax%=tempmax%+5 : IF ratioP%>50 THEN tempmax%=tempmax%+10
30270 temp!=INT( 28 + RND * tempmax% )
30280 LOCATE LPUS%, CPUS%, 1 : IF i < BP% THEN ratio%=INT(100*i/BARMAX%) : GOSUB 39000 : PRINT STRING$(i, OMB%); : PRINT TAB(45);" ] "; : PRINT USING "###";ratioP%; : PRINT TAB(54);"%"
30290 LOCATE LPTS%, CPTS%, 1   : PRINT USING "##.#";tension!;   : PRINT TAB(54);"V"
30300 LOCATE LPFQ%, CPFQ%, 1   : PRINT USING "##.#";frequence!; : PRINT TAB(54);"MHz"
30310 LOCATE LPTP%, CPTP%, 1   : PRINT USING "##.#";temp!;      : PRINT TAB(54);CHR$(248);"C"
30320 LOCATE LMUS%, CMUS%, 1 : IF i < BM% THEN ratio%=INT(100*i/BARMAX%) : GOSUB 39000 : PRINT STRING$(i, OMB%); : PRINT TAB(45);" ] ";SPACE$(1); : PRINT USING "##";INT(MEMMAX%*ratioM%/100); : PRINT TAB(54);"Ko"
30330 LOCATE LVIR%, CVIR%, 1 : IF i < 25  THEN ratio%=INT(100*i/24)      : GOSUB 39000 : PRINT STRING$(i, OMB%); : PRINT TAB(45);" ] "; : PRINT USING "###";100; : PRINT TAB(54);"%"
30340 LOCATE LVER%, CVER%, 1 : IF i < 25  THEN ratio%=INT(100*i/24)      : GOSUB 39000 : PRINT STRING$(i, OMB%); : PRINT TAB(45);" ] "; : PRINT USING "###";100; : PRINT TAB(54);"%"
30350 LOCATE LRNC%, CRNC%, 1 : IF i < 17  THEN ratio%=INT(100*i/16)      : GOSUB 39000 : PRINT STRING$(i, OMB%); : PRINT TAB(45);" ] "; : PRINT USING "###";100; : PRINT TAB(54);"%"
30360 FOR j= 1 TO duree: NEXT j : NEXT i

31010 LOCATE 20, 1 : PRINT "Appuyez sur CR/Entr";CHR$(130);"e pour continuer (une autre touche pour quitter)" : K$ = INPUT$(1)
31020 IF K$=CHR$(13) THEN GOTO 30030
31030 END

38999 REM ######## GOSUB et fonctions ########
39000 REM ***** Calcul l'ombre *****
39010 REM 176 <=> ombre légère ; 177 <=> ombre moyenne ; 178 <=> ombre sombre ; 219 <=> bloc plein
39020 IF ratio% > 75 THEN OMB%= 219 ELSE IF ratio% > 50 THEN OMB%= 178 ELSE IF ratio% > 25 THEN OMB%= 177 ELSE OMB%= 176
39030 RETURN
