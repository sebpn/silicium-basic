50000 CLS
50010 REM ***********************************************
50011 REM TRANSFERT - PROGRAMME GW BASIC
50012 REM Auteurs : PERIN S.
50013 REM Silicium 2023
50014 REM ************************************************

50020 CLS:print "5.	Proc";CHR$(130);"dure de transfert": PRINT
50030 PRINT "Appuyez sur CR/Entr";CHR$(130);"e pour commencer le transfert" : K$ = INPUT$(1)
50040 duree=3000 
50049 REM ligne=[1,25] col=[1,40]
50050 PRINT "Depuis l'espace virtuel : "; : LV%=CSRLIN : CV%=POS(0) : PRINT 
50060 PRINT "Vers l'espace euclidien : "; : LE%=CSRLIN : CE%=POS(0) : PRINT 
50070 FOR i=1 TO 20 
50080 LOCATE LV%, CV%, 1 : PRINT SPACE$(20) 
50090 LOCATE LV%, CV%, 1 : PRINT STRING$(20-i,178); : REM 178 <=> ombre sombre
50100 LOCATE LE%, CE%, 1 : PRINT STRING$(i,178);	
50110 FOR j= 1 TO duree: NEXT j:NEXT i : PRINT " Ok" : BEEP
50120 PRINT:PRINT "Transfert termin";CHR$(130);". Vous pouvez poursuivre"
50130 END