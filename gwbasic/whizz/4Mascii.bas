10 REM ***********************************************
20 REM ASCII ART - PROGRAMME GW BASIC
30 REM Auteurs : PERIN S.
40 REM Silicium 2023
50 REM ************************************************
40000 rem inspiration : https://www.asciiart.eu/computers
40010 rem
40020 REM ASCII Arts
40030 rem
40040 rem       ╔═══════════════════════╗
40050 rem       ║┌────────────────────┐ ║
40060 rem       ║│Silicium            │ ║
40070 rem       ║│Machines & Signaux  │ ║
40080 rem       ║│_                   │ ║
40090 rem ♫┌──┐ ║│                    │ ║ ┌──┐♫
40100 rem  │::│ ║│                    │o║ │::│ ♫
40110 rem  │::│ ║└────────────────────┘☼║ │::│
40120 rem  └──┘ ╚═══╦═══════════════╦═══╝ └──┘
40130 rem ╔═════════╩═══════════════╩═════════╗
40140 rem ║                       ┌──────────┐║
40150 rem ║ │││││││ o             │──▀████▀──│║
40160 rem ║ │││││││ ☼             └──────────┘║--.
40170 rem ╚═══════════════════════════════════╝-. \
40180 rem  ┌─────────────────────────────────┐-./  \
40190 rem  │▀ ▀ ▀  ▀ ▀ ▀  ▀ ▀ ▀  ▀ ▀ ▀  ▀ ▀ ▀│      )
40200 rem  │▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ █ ▀ ▀ ▀│     /T\
40210 rem  │▀ ▀ ▀ ▀ ▀▀▀▀▀▀▀▀▀ ▀ ▀ ▀ ▀   ▀ ▀ ▀│     \_/
40220 rem  └─────────────────────────────────┘
40230 rem
40240 PRINT SPACE$(7)+CHR$(201)+STRING$(23,205)+CHR$(187)
40250 PRINT SPACE$(7)+CHR$(186)+CHR$(218)+STRING$(20,196)+CHR$(191)+" "+CHR$(186)
40260 PRINT SPACE$(7)+CHR$(186)+CHR$(179)+"Silicium            "+CHR$(179)+" "+CHR$(186)
40270 PRINT SPACE$(7)+CHR$(186)+CHR$(179)+"Machines & Signaux  "+CHR$(179)+" "+CHR$(186)
40280 PRINT SPACE$(7)+CHR$(186)+CHR$(179)+"_                   "+CHR$(179)+" "+CHR$(186)
40290 PRINT " "+CHR$(14)+CHR$(218)+STRING$(2,196)+CHR$(191)+" "+CHR$(186)+CHR$(179)+SPACE$(20)+CHR$(179)+" "+CHR$(186)+" "+CHR$(218)+STRING$(2,196)+CHR$(191)+CHR$(14)
40300 PRINT SPACE$(2)+CHR$(179)+STRING$(2,58)+CHR$(179)+" "+CHR$(186)+CHR$(179)+SPACE$(20)+CHR$(179)+"o"+CHR$(186)+" "+CHR$(179)+STRING$(2,58)+CHR$(179)+" "+CHR$(14)
40310 PRINT SPACE$(2)+CHR$(179)+STRING$(2,58)+CHR$(179)+" "+CHR$(186)+CHR$(192)+STRING$(20,196)+CHR$(217)+CHR$(15)+CHR$(186)+" "+CHR$(179)+STRING$(2,58)+CHR$(179)
40320 PRINT SPACE$(2)+CHR$(192)+STRING$(2,196)+CHR$(217)+" "+CHR$(200)+STRING$(3,205)+CHR$(203)+STRING$(15,205)+CHR$(203)+STRING$(3,205)+CHR$(188)+" "+CHR$(192)+STRING$(2,196)+CHR$(217)
40330 PRINT " "+CHR$(201)+STRING$(9,205)+CHR$(202)+STRING$(15,205)+CHR$(202)+STRING$(9,205)+CHR$(187)
40340 PRINT " "+CHR$(186)+SPACE$(23)+CHR$(218)+STRING$(10,196)+CHR$(191)+CHR$(186)
40350 PRINT " "+CHR$(186)+" "+STRING$(7,179)+" o"+SPACE$(13)+CHR$(179)+STRING$(2,196)+CHR$(223)+STRING$(4,219)+CHR$(223)+STRING$(2,196)+CHR$(179)+CHR$(186)
40360 PRINT " "+CHR$(186)+" "+STRING$(7,179)+" "+CHR$(15)+SPACE$(13)+CHR$(192)+STRING$(10,196)+CHR$(217)+CHR$(186)+"--."
40370 PRINT " "+CHR$(200)+STRING$(35,205)+CHR$(188)+"-. \"
40380 PRINT SPACE$(2)+CHR$(218)+STRING$(33,196)+CHR$(191)+"-./  \"
40390 PRINT SPACE$(2)+CHR$(179); : FOR i=1 TO 4: PRINT CHR$(223)+" "+CHR$(223)+" "+CHR$(223)+"  "; :NEXT i : PRINT CHR$(223)+" "+CHR$(223)+" "+CHR$(223)+CHR$(179)+"      )"
40400 PRINT SPACE$(2)+CHR$(179); : FOR i=1 TO 13: PRINT CHR$(223)+" "; :NEXT i : PRINT CHR$(219)+" "+CHR$(223)+" "+CHR$(223)+" "+CHR$(223)+CHR$(179)+"     /T\"
40410 PRINT SPACE$(2)+CHR$(179); : FOR i=1 TO 4: PRINT CHR$(223)+" "; :NEXT i : PRINT STRING$(9,223); : FOR j=1 TO 4: PRINT " "+CHR$(223); :NEXT j : PRINT "   "+CHR$(223)+" "+CHR$(223)" "+CHR$(223)+CHR$(179)+"     \_/"
40420 PRINT SPACE$(2)+CHR$(192)+STRING$(33,196)+CHR$(217)







