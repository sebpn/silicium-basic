20000 REM ***********************************************
20010 REM QUEL NOMBRE - PROGRAMME GW BASIC
20020 REM
20030 REM Auteurs : PERIN S.
20040 REM Silicium 2023
20050 REM
20060 REM ************************************************
20070 RANDOMIZE TIMER
20080 NB% = INT( 1 + RND * 10 )
20090 CLS
20100 PRINT "Vous avez 3 essais pour trouver un nombre entre 1 et 10", CHR$(13);
20110 FOR n = 1 TO 3
20120 	PRINT "Quel est le nombre"; 
20130	INPUT R%
20140  	IF R% = NB% THEN GOTO 160 
20150   	IF R% > NB% THEN PRINT "C'est moins!"
20160   	IF R% < NB% THEN PRINT "C'est plus!"
20170     IF n < 3 THEN PRINT "Encore ", 3-n, "essai(s)"
20180 NEXT n
20190 PRINT "Perdu, il fallait trouver : ", NB%
20200 GOTO 170
20210 PRINT "Gagn"+CHR$(130);"
20220 END