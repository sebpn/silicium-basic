40000 REM ***********************************************
40010 REM CONCEPTION WHIZZ MACHINE- PROGRAMME GW BASIC
40011 REM Auteurs : PERIN S.
40012 REM Silicium 2023
40013 REM ************************************************
40014 credits%=100 : duree=1000 
40015 ON ERROR GOTO 47000
40016 DIM machine(8), mchntext$(8) : rem 0=proc, 1=mem, 2=disq, 3=hdd, 4=ecran, 5=clavier, 6=souris, 7=carteExt

40020 CLS:PRINT "4.	Conception d'une Whizz Machine": PRINT
40030 PRINT "Vous avez ";credits%;"("CHR$(155);CHR$(157);") cybercr";CHR$(130);"dits ": PRINT "pour construire une whizz machine fonctionnelle": PRINT
40040 PRINT " 1. Configurez l'unit";CHR$(130);" centrale"
40050 PRINT " 2. Configurez les interfaces"
40060 PRINT " 3. Testez votre machine"
40070 PRINT " 4. R";CHR$(130);"initialiser"
40080 PRINT " 5. Quitter"
40090 PRINT : INPUT "Votre choix ",I
40100 IF INT(I)<>I OR I<1 OR I>5 THEN GOTO 40020
40110 ON I GOTO 41000,42000,43000,44000,45000:GOTO 40020

41000 CLS:PRINT " 1. Configurez votre unit";CHR$(130);" centrale": PRINT 
41010 PRINT "Vous avez ";credits%;"("CHR$(155);CHR$(157);") cybercr";CHR$(130);"dits pour construire une whizz machine."
41020 PRINT "Par d";CHR$(130);"faut l'unit";CHR$(130);" centrale sera ";CHR$(130);"quip";CHR$(130);"e d'une alimentation ";CHR$(130);"lectrique"
41030 PRINT " et d'une carte m";CHR$(138);"re adapt";CHR$(130);"es." : PRINT
41035 PRINT "Configuration UC [ ";CHR$(230);"P= ";mchntext$(0);", memoire= ";mchntext$(1);", lecteur= ";mchntext$(2);", DisqueDur= ";mchntext$(3);"]"
41040 IF machine(0) = 0 THEN PRINT "Ajoutez un processeur :" : PRINT SPACE$(2) "1. Zilog Z80 ";CHR$(133);" 2MHz";TAB(40);"15";CHR$(155);CHR$(157) : PRINT SPACE$(2) "2. Motorola 68000 ";CHR$(133);" 8MHz";TAB(40);"20";CHR$(155);CHR$(157)
41050 IF machine(1) = 0 THEN PRINT "Ajoutez de la m";CHR$(130);"moire vive (RAM)" : PRINT SPACE$(2) "3. 16Ko";TAB(40);"15";CHR$(155);CHR$(157) : PRINT SPACE$(2) "4. 48Ko";TAB(40);"20";CHR$(155);CHR$(157)
41060 IF machine(2) = 0 THEN PRINT "Ajoutez un lecteur" : PRINT SPACE$(2) "5. disquette 5";CHR$(34);CHR$(172);" HD";TAB(40);"5";CHR$(155);CHR$(157) : PRINT SPACE$(2) "6. disquette 3";CHR$(34);CHR$(171);TAB(40);"10";CHR$(155);CHR$(157)
41070 IF machine(3) = 0 THEN PRINT "Ajoutez un disque dur" : PRINT SPACE$(2) "7. 5Mo";TAB(40);"85";CHR$(155);CHR$(157) : PRINT SPACE$(2) "8. 10Mo";TAB(40);"95";CHR$(155);CHR$(157)
41080 PRINT "9. Retour au menu"
41090 PRINT : INPUT "Votre choix ",I
41100 IF INT(I)<>I OR I<1 OR I>9 THEN GOTO 41000
41110 ON I GOTO 41120,41125,41130,41135,41140,41145,41150,41155,40020:GOTO 41000
41120 IF machine(0) > 0 THEN GOTO 41000 ELSE cout%=15 : GOSUB 46000 : machine(0) = I : mchntext$(0) = "Zilog Z80"      : GOTO 41160 : REM Test budget
41125 IF machine(0) > 0 THEN GOTO 41000 ELSE cout%=20 : GOSUB 46000 : machine(0) = I : mchntext$(0) = "Motorola 68000" : GOTO 41160 : REM Test budget
41130 IF machine(1) > 0 THEN GOTO 41000 ELSE cout%=15 : GOSUB 46000 : machine(1) = I : mchntext$(1) = "16Ko"           : GOTO 41160 : REM Test budget
41135 IF machine(1) > 0 THEN GOTO 41000 ELSE cout%=20 : GOSUB 46000 : machine(1) = I : mchntext$(1) = "48Ko"           : GOTO 41160 : REM Test budget
41140 IF machine(2) > 0 THEN GOTO 41000 ELSE cout%=5  : GOSUB 46000 : machine(2) = I : mchntext$(2) = "disquette 5"+CHR$(34)+CHR$(172)+" HD" : GOTO 41160 : REM Test budget
41145 IF machine(2) > 0 THEN GOTO 41000 ELSE cout%=10 : GOSUB 46000 : machine(2) = I : mchntext$(2) = "disquette 3"+CHR$(34)+CHR$(171)       : GOTO 41160 : REM Test budget
41150 IF machine(3) > 0 THEN GOTO 41000 ELSE cout%=85 : GOSUB 46000 : machine(3) = I : mchntext$(3) = "5Mo"            : GOTO 41160 : REM Test budget
41155 IF machine(3) > 0 THEN GOTO 41000 ELSE cout%=95 : GOSUB 46000 : machine(3) = I : mchntext$(3) = "10Mo"           : GOTO 41160 : REM Test budget
41160 credits%=credits%-cout% 
41170 FOR i=1 to 20 : PRINT CHR$(178); :FOR j= 1 TO duree: NEXT j: NEXT i : PRINT " Ok" :FOR j= 1 TO duree: NEXT j
41180 GOTO 41000

42000 CLS:PRINT " 2. Configurez vos interfaces": PRINT
42010 PRINT "Vous avez ";credits%;"("CHR$(155);CHR$(157);") cybercr";CHR$(130);"dits pour construire une whizz machine." : PRINT
42015 PRINT "Configuration interfaces [ ecran= ";mchntext$(4);", clavier= ";mchntext$(5);", souris= ";mchntext$(6);", extension= ";mchntext$(7);"]"
42020 IF machine(4) = 0 THEN PRINT "Ajoutez un ";CHR$(130);"cran" : PRINT SPACE$(2) "1. 25x40 Monochrome";TAB(40);"15";CHR$(155);CHR$(157) : PRINT SPACE$(2) "2. 25x80 16 Couleurs";TAB(40);"20";CHR$(155);CHR$(157)
42030 IF machine(5) = 0 THEN PRINT "Ajoutez un clavier m";CHR$(130);"canique" : PRINT SPACE$(2) "3. 83 touches";TAB(40);"8";CHR$(155);CHR$(157) : PRINT SPACE$(2) "4. 101 touches";TAB(40);"10";CHR$(155);CHR$(157)
42040 IF machine(6) = 0 THEN PRINT "Ajoutez une souris" : PRINT SPACE$(2) "5. 2 boutons";TAB(40);"10";CHR$(155);CHR$(157)
42050 IF machine(7) = 0 THEN PRINT "Ajoutez une carte d'extension" : PRINT SPACE$(2) "6. carte son st";CHR$(130);"r";CHR$(130);"phonique";TAB(40);"20";CHR$(155);CHR$(157) : PRINT SPACE$(2) "7. carte r";CHR$(130);"seau";TAB(40);"15";CHR$(155);CHR$(157)
42060 PRINT "8. Retour au menu"
42070 PRINT : INPUT "Votre choix ",I
42080 IF INT(I)<>I OR I<1 OR I>8 THEN GOTO 42000
42090 ON I GOTO 42100,42105,42110,42115,42120,42130,42135,40020:GOTO 42000
42100 IF machine(4) > 0 THEN GOTO 41000 ELSE cout%=15 : GOSUB 46000 : machine(4) = I : mchntext$(4) = "25x40 Monochrome"  : GOTO 42140 : REM Test budget
42105 IF machine(4) > 0 THEN GOTO 41000 ELSE cout%=20 : GOSUB 46000 : machine(4) = I : mchntext$(4) = "25x80 16 Couleurs" : GOTO 42140 : REM Test budget
42110 IF machine(5) > 0 THEN GOTO 41000 ELSE cout%=8  : GOSUB 46000 : machine(5) = I : mchntext$(5) = "83 touches"        : GOTO 42140 : REM Test budget
42115 IF machine(5) > 0 THEN GOTO 41000 ELSE cout%=10 : GOSUB 46000 : machine(5) = I : mchntext$(5) = "101 touches"       : GOTO 42140 : REM Test budget
42120 IF machine(6) > 0 THEN GOTO 41000 ELSE cout%=10 : GOSUB 46000 : machine(6) = I : mchntext$(6) = "2 boutons"         : GOTO 42140 : REM Test budget
42130 IF machine(7) > 0 THEN GOTO 41000 ELSE cout%=20 : GOSUB 46000 : machine(7) = I : mchntext$(7) = "carte son"         : GOTO 42140 : REM Test budget
42135 IF machine(7) > 0 THEN GOTO 41000 ELSE cout%=15 : GOSUB 46000 : machine(7) = I : mchntext$(7) = "carte reseau"      : GOTO 42140 : REM Test budget
42140 credits%=credits%-cout% 
42150 FOR i=1 TO 20 : PRINT CHR$(178); :FOR j= 1 TO duree: NEXT j: NEXT i : PRINT " Ok" :FOR j= 1 TO duree: NEXT j
42160 GOTO 42000

43000 CLS:PRINT " 3. Testez votre machine": PRINT
43010 PRINT "Appuyez sur CR/Entr";CHR$(130);"e pour lancer le test" : K$ = INPUT$(1)
43020 IF machine(0)=0 AND machine(1)=0 AND machine(2)=0 AND machine(3)=0 AND machine(4)=0 AND machine(5)=0 AND machine(6)=0 AND machine(7)=0 THEN GOTO 43030 ELSE GOTO 43050
43030 PRINT "Il n'y a pas de configuration ";CHR$(133);" tester ! Appuyez sur CR/Entr";CHR$(130);"e" : K$ = INPUT$(1)
43040 GOTO 40020
43050 FOR i=1 TO 20 : PRINT CHR$(178); :FOR j= 1 TO duree: NEXT j: NEXT i : PRINT " Ok" :FOR j= 1 TO duree: NEXT j : PRINT
43060 IF machine(0)>0 AND machine(1)>0 AND machine(2)>0 AND machine(4)>0 AND machine(5)>0 THEN GOTO 44999
43070 print "Votre machine n'est pas compl";CHR$(138);"te, il manque :"
43080 IF machine(0) < 1 THEN PRINT " - un processeur" 
43090 IF machine(1) < 1 THEN PRINT " - une m";CHR$(130);"moire" 
43100 IF machine(2) < 1 THEN PRINT " - un lecteur de disquette" 
43110 IF machine(4) < 1 THEN PRINT " - un ";CHR$(130);"cran" 
43120 IF machine(5) < 1 THEN PRINT " - un clavier" 
43130 PRINT "Appuyez sur CR/Entr";CHR$(130);"e" : K$ = INPUT$(1)
43999 GOTO 40020

44000 PRINT " 4. R";CHR$(130);"initialiser" : PRINT
44010 credits%=100 : ERASE machine,mchntext$ : DIM machine(8), mchntext$(8)
44020 FOR i=1 TO 20 : PRINT CHR$(178); :FOR j= 1 TO duree: NEXT j: NEXT i : PRINT " Ok" :FOR j= 1 TO duree: NEXT j
44030 GOTO 40020

44999 PRINT "Test Ok" : BEEP
45000 PRINT "La conception est termin";CHR$(130);"e."
45010 END

45999 REM ######## GOSUB et fonctions ########
46000 REM ***** Test budget *****
46010 IF (credits%-cout%)<0 THEN PRINT "Vous n'avez pas le budget, veuillez choisir un composant mois on";CHR$(130);"reux ou r";CHR$(130);"initialiser votre configuration": K$ = INPUT$(1) : GOTO 40020
46020 RETURN
47000 REM ***** GESTION ERREUR *****
47010 REM PRINT "Err #";ERR
47020 IF ERR=10 THEN ERASE machine,mchntext$ : DIM machine(8), mchntext$(8)
47030 RESUME NEXT
